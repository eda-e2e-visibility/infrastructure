CREATE STREAM OrderWorkflowSteps(stepName VARCHAR, completionTime BIGINT) WITH (KAFKA_TOPIC='order-workflow-step-completed', VALUE_FORMAT='AVRO');


CREATE TABLE OrderEndToEndFlowSteps WITH(VALUE_AVRO_SCHEMA_FULL_NAME='com.streams.samples.OrderEndToEndFlowSteps') AS
SELECT rowkey as OrderId, collect_list(stepName)       AS workflowSteps,
       TIMESTAMPTOSTRING(min(completionTime), 'HH:mm:ss:SSS') AS timeOfFirstCompletedStep,
       TIMESTAMPTOSTRING(max(completionTime), 'HH:mm:ss:SSS') AS timeOfLastCompletedStep
FROM OrderWorkflowSteps
GROUP BY ROWKEY
EMIT CHANGES;

select collect_list(stepName) AS workflowSteps, collect_list(completionTime) as workflowStepCompletionTimes

from OrderWorkflowSteps group by ROWKEY emit changes;

SELECT * FROM OrderWorkflowSteps emit changes;

CREATE STREAM ExpiredOrders(expiry BIGINT, lastCompletedWorkflowStep VARCHAR ) WITH (KAFKA_TOPIC='order-expired', VALUE_FORMAT='AVRO');
SELECT * FROM ExpiredOrders emit changes;




SELECT ROWKEY as orderId, timeOfFirstCompletedStep, timeOfLastCompletedStep, workflowSteps
FROM OrderEndToEndFlow EMIT CHANGES;