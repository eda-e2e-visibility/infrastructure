#!/usr/bin/env bash

./stop.sh

docker-compose -f docker-compose-kafka.yml -f docker-compose-others.yml up -d

docker exec -i grafana sh -c 'grafana-cli plugins install grafana-piechart-panel'
docker exec -i grafana sh -c 'grafana-cli plugins install mtanda-histogram-panel'
docker restart grafana

# Need to wait for the Kafka topics to be created and the ksqldb-server to be ready
sleep 120

./scripts/ksqldb-init.sh
