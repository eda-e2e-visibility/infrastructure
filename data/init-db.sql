create table "order-placed"
(
	"totalPaidAmount" text not null,
	"orderTime" bigint not null,
	"shopId" text not null,
	"userId" text not null,
	key text not null primary key
);

alter table "order-placed" owner to kafka;


create table "order-completed"
(
	"completedTime" bigint not null,
	key text not null primary key
);

alter table "order-completed" owner to kafka;

create table "order-workflow-step-completed"
(
	"completionTime" bigint not null,
	"previousWorkflowStep" text,
	"stepName" text not null,
	key text not null,
	id SERIAL PRIMARY KEY
);

alter table "order-workflow-step-completed" owner to kafka;

create table "order-expired"
(
	expiry bigint not null,
	key text not null primary key
);

alter table "order-expired" owner to kafka;

