#!/usr/bin/env bash

# Wait for KSQL server to be up and running
bash -c 'while [[ "$(curl -s -o /dev/null -w ''200'' http://localhost:8088/healthcheck)" != "200" ]]; do sleep 5; done'

#Create OrderWorkflowSteps streams out of the existing topic
curl -X "POST" "http://localhost:8088/ksql" \
     -H "Content-Type: application/vnd.ksql.v1+json; charset=utf-8" \
     -d $'{
  "ksql": "CREATE STREAM OrderWorkflowSteps(stepName VARCHAR, completionTime BIGINT) WITH (KAFKA_TOPIC=\'order-workflow-step-completed\', VALUE_FORMAT=\'AVRO\');",
  "streamsProperties": {}
}'


curl -X "POST" "http://localhost:8088/ksql" \
     -H "Content-Type: application/vnd.ksql.v1+json; charset=utf-8" \
     -d $'{
  "ksql": "CREATE TABLE OrderEndToEndFlowSteps WITH(VALUE_AVRO_SCHEMA_FULL_NAME=\'com.streams.samples.OrderEndToEndFlowSteps\') AS SELECT collect_list(stepName) AS workflowSteps, collect_list(completionTime) as workflowStepCompletionTimes, min(completionTime) AS timeOfFirstCompletedStep, max(completionTime) AS timeOfLastCompletedStep FROM OrderWorkflowSteps GROUP BY ROWKEY;",
  "streamsProperties": {}
}'

curl -X "POST" "http://localhost:8088/ksql" \
     -H "Content-Type: application/vnd.ksql.v1+json; charset=utf-8" \
     -d $'{
  "ksql": "CREATE SINK CONNECTOR SINK_OTHER_ORDER_EVENTS WITH (\'connector.class\'  = \'io.confluent.connect.jdbc.JdbcSinkConnector\',\'key.converter.schema.registry.url\'  = \'http://schema-registry:8081\',\'value.converter.schema.registry.url\'  = \'http://schema-registry:8081\',\'connection.url\'  = \'jdbc:postgresql://postgres:5432/kafka-sink\',\'connection.user\' = \'kafka\',\'connection.password\'  = \'pass\',\'topics\' = \'order-completed,order-workflow-step-completed\',\'value.converter\'  = \'io.confluent.connect.avro.AvroConverter\',\'key.converter\'   = \'io.confluent.connect.avro.AvroConverter\',\'internal.key.converter\' = \'org.apache.kafka.connect.json.JsonConverter\',\'internal.value.converter\'= \'org.apache.kafka.connect.json.JsonConverter\',\'transforms\' = \'ExtractKeyId,InsertKeyField\',\'transforms.ExtractKeyId.type\' = \'org.apache.kafka.connect.transforms.ExtractField$Key\',\'transforms.ExtractKeyId.field\' = \'id\',\'transforms.InsertKeyField.type\' = \'com.hussein.samples.kafka.connect.smt.InsertKeyIntoValue$Value\',\'transforms.InsertKeyField.field.name\' = \'key\');",
  "streamsProperties": {}
}'

curl -X "POST" "http://localhost:8088/ksql" \
     -H "Content-Type: application/vnd.ksql.v1+json; charset=utf-8" \
     -d $'{
  "ksql": "CREATE SINK CONNECTOR SINK_ORDER_PLACED_EVENTS WITH (\'connector.class\'  = \'io.confluent.connect.jdbc.JdbcSinkConnector\',\'key.converter.schema.registry.url\'  = \'http://schema-registry:8081\',\'value.converter.schema.registry.url\'  = \'http://schema-registry:8081\',\'connection.url\'  = \'jdbc:postgresql://postgres:5432/kafka-sink\',\'connection.user\' = \'kafka\',\'connection.password\'  = \'pass\',\'topics\' = \'order-placed\',\'value.converter\'  = \'io.confluent.connect.avro.AvroConverter\',\'key.converter\'   = \'io.confluent.connect.avro.AvroConverter\',\'internal.key.converter\' = \'org.apache.kafka.connect.json.JsonConverter\',\'internal.value.converter\'= \'org.apache.kafka.connect.json.JsonConverter\',\'transforms\' = \'ExtractKeyId,ReplaceField,InsertKeyField\',\'transforms.ExtractKeyId.type\' = \'org.apache.kafka.connect.transforms.ExtractField$Key\',\'transforms.ExtractKeyId.field\' = \'id\',\'transforms.ReplaceField.type\'= \'org.apache.kafka.connect.transforms.ReplaceField$Value\',\'transforms.ReplaceField.blacklist\' = \'orderItems\',        \'transforms.InsertKeyField.type\' = \'com.hussein.samples.kafka.connect.smt.InsertKeyIntoValue$Value\',\'transforms.InsertKeyField.field.name\' = \'key\');",
  "streamsProperties": {}
}'

curl -X "POST" "http://localhost:8088/ksql" \
     -H "Content-Type: application/vnd.ksql.v1+json; charset=utf-8" \
     -d $'{
  "ksql": "CREATE SINK CONNECTOR SINK_ORDER_EXPIRED_EVENTS WITH (\'connector.class\'  = \'io.confluent.connect.jdbc.JdbcSinkConnector\',\'key.converter.schema.registry.url\'  = \'http://schema-registry:8081\',\'value.converter.schema.registry.url\'  = \'http://schema-registry:8081\',\'connection.url\'  = \'jdbc:postgresql://postgres:5432/kafka-sink\',\'connection.user\' = \'kafka\',\'connection.password\'  = \'pass\',\'topics\' = \'order-expired\',\'value.converter\'  = \'io.confluent.connect.avro.AvroConverter\',\'key.converter\'   = \'io.confluent.connect.avro.AvroConverter\',\'internal.key.converter\' = \'org.apache.kafka.connect.json.JsonConverter\',\'internal.value.converter\'= \'org.apache.kafka.connect.json.JsonConverter\',\'transforms\' = \'ExtractKeyId,ReplaceField,InsertKeyField\',\'transforms.ExtractKeyId.type\' = \'org.apache.kafka.connect.transforms.ExtractField$Key\',\'transforms.ExtractKeyId.field\' = \'id\',\'transforms.ReplaceField.type\'= \'org.apache.kafka.connect.transforms.ReplaceField$Value\',\'transforms.ReplaceField.blacklist\' = \'lastCompletedWorkflowStep\',        \'transforms.InsertKeyField.type\' = \'com.hussein.samples.kafka.connect.smt.InsertKeyIntoValue$Value\',\'transforms.InsertKeyField.field.name\' = \'key\');",
  "streamsProperties": {}
}'

