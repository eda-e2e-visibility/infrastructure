# Infrastructure for Business process observability

An infrastructure for the business process end-to-end workflow/observability prototype.
All necessary services are defined in the provided docker compose files.

## How to run

To start the services you might need to run `restart.sh`. This script does the following

* Start required docker containers
* Install required Grafana plugins
* Create KSQL stream and table that are used in the `order-monitoring-service`.

## Monitoring setup

Prometheus is configured to get metrics from the `order-monitoring-service` running in `host.docker.internal:8888`(Why [host.docker.internal](https://docs.docker.com/docker-for-mac/networking/)).
The `order-monitoring-service` is using spring-metrics to export some custom metrics related to order workflow tracking.
Grafana is configured to extract those metrics to draw a dashboard like the following

<p align="center">
  <img  width="700" src="docs/order-workflow-grafana-dashboard.png?raw=true" title="Order monitoring Grafana dashboard">
</p>

## Future improvements

* Wait for Kafka topics to be created. For the KsqlDB scripts to run, the used Kafka topics have to be created.
For that reason, I am sleeping for 2 minutes before running the KsqlDB scripts. There should be a way to check the topics and continue the script once the preconditions are met.
